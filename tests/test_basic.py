from jinja_play.app import main


def test_basic_tmpl():
    main()
    from jinja_play.cmps_dmodels import (
        Ahu,
        Coil,
        Damper,
        Drain_pan,
        Filter,
        Phex,
    )

    Ahu()
    Coil()
    Damper()
    Drain_pan()
    Filter()
    Phex()
