import pathlib

from jinja2 import Template
import pandas as pd
from envparse import env

from .utils import (
    authorize_access_to_gspread,
    get_df_from_gsheet,
    preprocess_data,
)
from .templates import DATAMODEL_TMPL


DATA_SHEET_KEY = (
    "1Ti8TMPNiRD_2gDZwYES_9nBoR9GFg5kPpCcXg57Pn2M"
)
DATA_SOURCE_SHEET_NAME = "DATA"


env.read_envfile()
wkb = authorize_access_to_gspread(DATA_SHEET_KEY)
wks = wkb.worksheet(DATA_SOURCE_SHEET_NAME)

data_raw = get_df_from_gsheet(wks)


def gen_datamodel(
    target_file: pathlib.Path, template: str
) -> None:

    template_ = Template(template)
    datamodels_content = template_.render(
        variables=preprocess_data(data_raw)
    )
    with open(target_file, "w") as f:
        f.writelines(datamodels_content)
    return


def main():
    gen_datamodel(
        pathlib.Path(__file__).parent
        / "cmps_dmodels.py",
        DATAMODEL_TMPL,
    )
