DATAMODEL_TMPL = """import attr


{% for comp in variables.keys() -%}
@attr.s(auto_attribs=True)
class {{ comp.capitalize() }}:
    {% 
    for v_name, 
    v_val, 
    v_type, 
    v_default 
    in variables[comp] 
    -%}
    {{ v_name }}: {{ v_type }} = attr.ib(default={{ v_default }})
    {% endfor %}

{% endfor %}
"""
