from typing import Dict

import gspread
from oauth2client.service_account import (
    ServiceAccountCredentials,
)
import pandas as pd
import numpy as np
from envparse import env
from slugify import slugify


TYPE_DICT = {
    "real": "float",
    "int": "int",
    "bool": "bool",
    "": "str",
}
COMPONENT_DICT = {
    "am": "ahu",
    "fan": "fan",
    "filtr": "filter",
    "COIL": "coil",
    "KLP": "damper",
    "vana": "drain_pan",
    "DEV": "phex",
}
DEFAULT_DICT = {
    "int": "0",
    "float": "0.0",
    "bool": "False",
    "str": "''",
}


def authorize_access_to_gspread(
    spreadsheet_key: str,
) -> gspread.Spreadsheet:
    """
    Authorize access to the given spreadsheet
    :param spreadsheet_key: A key of the spreadsheet.
        Might be found after /d of the spreadsheet URL
        For example if the URL is: https://docs.google.com/
            spreadsheets/d/2l-zdfdfNCBWh-Cu1-m-uaybz2aVVqtfJM6cHGGd[QgSS79sw/
        then key = "2l-zdfdfNCBWh-Cu1-m-uaybz2aVVqtfJM6cHGGd[QgSS79sw"
    :return: an instance of Spreadsheet
    """
    scope = ["https://spreadsheets.google.com/feeds"]
    # noinspection PyProtectedMember,PyPep8
    credentials = ServiceAccountCredentials._from_parsed_json_keyfile(
        {
            "type": "service_account",
            "project_id": "ahu-platform",
            "private_key_id": env.str(
                "GSHEET_PRIVATE_KEY_ID"
            ),
            "private_key": env.str(
                "GSHEET_PRIVATE_KEY"
            ),
            "client_id": env.str("GSHEET_CLIENT_ID"),
            "client_email": env.str(
                "GSHEET_CLIENT_EMAIL"
            ),
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/admin-978%40ahu-platform.iam.gserviceaccount.com",
        },
        scope,
    )

    gc = gspread.authorize(credentials)
    wks = gc.open_by_key(spreadsheet_key)
    return wks


def get_df_from_gsheet(
    wks: gspread.Worksheet,
) -> pd.DataFrame:
    data_vals = wks.get_all_values()
    return pd.DataFrame(
        data_vals[4:], columns=data_vals[0]
    )


def preprocess_data(
    data_raw: pd.DataFrame
) -> Dict[str, np.array]:
    data = data_raw[
        (data_raw["Sloupec1"] != "")
        & (data_raw["proměnná"] != "")
        & (data_raw["hodnota"] != "")
    ]

    data_clean = data.loc[
        :,
        [
            "Sloupec1",
            "proměnná",
            "hodnota",
            "Sloupec3",
        ],
    ]

    data_clean["Sloupec3"] = data_clean[
        "Sloupec3"
    ].replace(TYPE_DICT)
    data_clean["Sloupec1"] = data_clean[
        "Sloupec1"
    ].replace(COMPONENT_DICT)
    data_clean["proměnná"] = data_clean[
        "proměnná"
    ].apply(lambda x: slugify(x, separator="_"))
    data_clean["default"] = data_clean[
        "Sloupec3"
    ].replace(DEFAULT_DICT)

    return {
        comp: data_clean[
            data_clean["Sloupec1"] == comp
        ]
        .loc[
            :,
            [
                "proměnná",
                "hodnota",
                "Sloupec3",
                "default",
            ],
        ]
        .to_numpy()
        for comp in data_clean["Sloupec1"].unique()
    }
